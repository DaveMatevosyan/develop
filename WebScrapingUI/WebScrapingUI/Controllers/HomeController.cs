﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using WebApi.Models;
using Newtonsoft.Json.Linq;
using System.Web.Routing;

namespace WebScrapingUI.Controllers
{
    public class HomeController : Controller
    {
        public HttpClient client = new HttpClient();

        public async Task<ActionResult> Index()
        {
            IEnumerable<string> sites = await GetAllNames();
            ViewData["Sites"] = sites;
            RequestModel m = new RequestModel();
            return View();
        }

        private async Task<IEnumerable<string>> GetAllNames()
        {
            Uri server_url = new Uri("https://localhost:44342/");
            client.BaseAddress = server_url;
            var response = await client.GetAsync("api/Scraper/GetSitesNames/");
            var contenteWithSlashes = await response.Content.ReadAsStringAsync();
            var strWithoutSlashes = JToken.Parse(contenteWithSlashes).ToString();
            List<string> sites =  JsonConvert.DeserializeObject<List<string>>(strWithoutSlashes);
            return sites;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(RequestModel model)
        {
            IEnumerable<string> sites = await GetAllNames();
            ViewData["Sites"] = sites;
            if (ModelState.IsValid)
            {
                foreach (string s in model.AllSites.Keys)
                {
                    if (model.AllSites[s] == true)
                        model.CheckedSites.Add(s);
                }
                if (model.CheckedSites.Count == 0)
                {
                    ModelState.AddModelError("", "at least one page must be selected");
                    return View();
                }
                if(model.MinPrice != null || model.MaxPrice != null)
                {
                    if(model.Currency == null)
                    {
                        ModelState.AddModelError("", "Currency must be selected");
                        return View();
                    }
                    if (model.MinPrice >= model.MaxPrice)
                    {
                        ModelState.AddModelError("", "minimum price must be less than maximum price");
                        return View();
                    }
                }
                TempData["Checked_sites"] = model.CheckedSites;
                return RedirectToAction("Result", "Result", model);
            }
            return View();
        } 
    }
}