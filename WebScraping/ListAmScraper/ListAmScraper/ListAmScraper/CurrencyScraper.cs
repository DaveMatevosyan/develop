﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ListAmScraper
{
    public class CurrencyScraper
    {
        public string SimpleUrl => "https://freecurrencyrates.com/en/#;;;;fcr";

        public async Task<double> StartScraping(string from, string to, int amount = 1)
        {
            string Url = SimpleUrl.Insert(SimpleUrl.IndexOf("#") + 1, from);
            Url = Url.Insert(Url.IndexOf(";") + 1, to);
            Url = Url.Insert(Url.IndexOf(to) + to.Length + 1, amount.ToString());

            //Process.Start("chrome.exe", Url);
            HttpClient client = new HttpClient();
            string strHtml;
            try
            {
                strHtml = await client.GetStringAsync(Url);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }

            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc?.LoadHtml(strHtml);

            HtmlNode value = htmlDoc.DocumentNode.Descendants("input")
                .Where(node => node.GetAttributeValue("id", "")
                .Equals("value_to")).FirstOrDefault();

            string exchange_rate = value.GetAttributeValue("value", "");
            double rate = -1;
            try
            {
                rate = double.Parse(exchange_rate.Replace('.', ','));
            }
            catch(FormatException e)
            {
                Console.WriteLine(e.Message);
                return rate;
            }

            return rate;
        }
    }
}
